package com.arsen.urlchecker

import android.app.AlertDialog
import android.app.Application
import android.app.Dialog
import android.arch.lifecycle.*
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.arsen.urlchecker.db.UrlDB
import kotlin.collections.ArrayList


class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: MainRepository = MainRepository
    var urlsLiveData: LiveData<List<UrlDB>>? = null

    init {
        urlsLiveData = repository.urlList
    }

    fun removeUrlItem(url: UrlDB) {
        repository.removeUrl(url)
    }

    fun sortListByChoose(urlList: ArrayList<UrlDB>, sortType:Int):List<UrlDB>?
    {
        return when(sortType){
            0->  urlList.sortedWith(compareBy(UrlDB::link))

            1->  urlList.sortedWith(compareBy(UrlDB::link)).reversed()

            2->  urlList.sortedWith(compareBy(UrlDB::connectionTime))

            3->  urlList.sortedWith(compareBy(UrlDB::connectionTime)).reversed()

            4-> urlList.sortedWith(compareBy(UrlDB::availability))

            else -> null
        }

    }

    fun refreshAllList(urlList : ArrayList<UrlDB>){
        repository.refreshList(urlList)
    }

    fun openDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.search_url)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val url = dialog.findViewById<EditText>(R.id.searchedUrl)
        dialog.findViewById<Button>(R.id.negative).setOnClickListener {
            dialog.cancel()
        }
        dialog.findViewById<Button>(R.id.positive).setOnClickListener {
            if (url.text.length>1) {
                val validUrl = "https://www.${url.text}"
                repository.checkURL(validUrl)
                dialog.cancel()
            } else {
                Toast.makeText(context, "Please Enter Valid Url,\n Like a https://www.something.com", Toast.LENGTH_LONG)
                    .show()
            }
        }
        dialog.show()

    }

    fun openRemoveDialog(context : Context,url:UrlDB){
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want delete item which doesn't updated yet?")
            .setCancelable(false)
            .setPositiveButton("Yes") {
                    dialog, which ->
                repository.removeUrl(url)
            }
            .setNegativeButton("No"){
                dialog, which ->
                dialog.cancel()
            }
        val alert = builder.create()
        alert.show()
    }
}