package com.arsen.urlchecker

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.arsen.urlchecker.adapter.UrlAdapter
import kotlinx.android.synthetic.main.activity_main.*
import android.view.*
import com.arsen.urlchecker.db.UrlDB


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    val list = ArrayList<UrlDB>()
    private var adapter: UrlAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolBar)
        urlsRecyclerView.layoutManager = LinearLayoutManager(this)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        addUrl.setOnClickListener {
            viewModel.openDialog(this)
        }
        drawRecyclerView()
        searchUrl()
    }

    private fun searchUrl() {
        searchUrlInList.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val newList = ArrayList<UrlDB>()
                for (url in list) {
                        if ( url.link.contains(s.toString())) {
                            newList.add(url)
                        }
                    adapter?.updateList(newList)
                }
            }

        })
    }

    private fun drawRecyclerView() {
        viewModel.urlsLiveData?.observe(this, Observer {
            list.addAll(it!!)
            adapter = UrlAdapter(ArrayList(it)) {
                if (it.availability == 0){
                        viewModel.openRemoveDialog(this,it)
                }else{
                    viewModel.removeUrlItem(it)
                    list.remove(it)
                }


            }
            urlsRecyclerView.adapter = adapter
            adapter!!.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            R.id.nameSortASC ->{
                adapter!!.updateList(viewModel.sortListByChoose(list,0)!!)
            }

            R.id.nameSortDESC ->{
                adapter!!.updateList(viewModel.sortListByChoose(list,1)!!)
            }

            R.id.timeSortASC->{
                adapter!!.updateList(viewModel.sortListByChoose(list,2)!!)
            }

            R.id.timeSortDESC ->{
                adapter!!.updateList(viewModel.sortListByChoose(list,3)!!)
            }

            R.id.availabilitySort -> {
                adapter!!.updateList(viewModel.sortListByChoose(list,4)!!)
            }

            R.id.refresh -> {
                    viewModel.refreshAllList(list)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
