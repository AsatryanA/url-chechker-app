package com.arsen.urlchecker.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arsen.urlchecker.R
import com.arsen.urlchecker.db.UrlDB
import kotlinx.android.synthetic.main.url_item.view.*

class UrlAdapter(var list: ArrayList<UrlDB>, val remove: (UrlDB) -> Unit) :
    RecyclerView.Adapter<UrlAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.url_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val url = list[position]
        holder.url.text = url.link
        when (url.availability) {
            0 -> {
                holder.image.visibility = View.GONE
                holder.loading.visibility = View.VISIBLE
            }
            200 -> {
                holder.loading.visibility = View.GONE
                holder.image.visibility = View.VISIBLE
                holder.image.setImageResource(R.drawable.success_icon)
            }
            else -> {
                holder.loading.visibility = View.GONE
                holder.image.visibility = View.VISIBLE
                holder.image.setImageResource(R.drawable.error_icon)
            }
        }

        holder.remove.setOnClickListener {
            remove(url)
        }

    }

    fun updateList(filteredList : List<UrlDB>){
        list.removeAll(list)
        list.addAll(filteredList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val url = view.url!!
        val image = view.urlResult!!
        val loading = view.loading!!
        val remove = view.remove!!

    }
}