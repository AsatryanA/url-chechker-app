package com.arsen.urlchecker

import android.arch.lifecycle.LiveData
import com.arsen.urlchecker.db.UrlDB
import com.arsen.urlchecker.db.dao.UrlDao
import com.arsen.urlchecker.service.NetworkCall
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

object MainRepository {


    private var networkCall: NetworkCall
    private var urlDao: UrlDao? = null
    var urlList : LiveData<List<UrlDB>>?=null


    init {
        val database = BaseApp().getDataBase()
        networkCall = NetworkCall()
        urlDao = database?.urlDao()
        urlList = urlDao?.getLiveUrl()

    }


    fun removeUrl(url: UrlDB) {
        Completable.fromAction {
            urlDao?.deleteUrl(url)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun checkURL(address: String) {
        val url = UrlDB(address, 0)
        addUrl(url)
        getResultOfUrl(address).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun refreshList(list: ArrayList<UrlDB>){
        for ( url in list){
//            getResultOfUrl(url.link).subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe()
        }
    }

    private fun getResultOfUrl(url: String): Observable<UrlDB> {
        return Observable.create {
            try {
                val response = networkCall.checkIfURLExists(url)
                it.onNext(response!!)
                it.onComplete()
                updateUrl(response)
            } catch (e: Exception) {
                it.onError(e)
            }
        }
    }

    private fun addUrl(url: UrlDB) {
        Completable.fromAction {
            urlDao?.addUrl(url)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    private fun updateUrl(urlObject : UrlDB) {
        Completable.fromAction {
            urlDao?.updateUrlField(urlObject.link,urlObject.connectionTime,urlObject.availability)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

}