package com.arsen.urlchecker.service

import android.util.Log
import com.arsen.urlchecker.db.UrlDB
import java.net.HttpURLConnection
import java.net.URL

class NetworkCall {


    fun checkIfURLExists(targetUrl: String): UrlDB? {
        val httpUrlConn: HttpURLConnection
        var connectionTime:Long = 0
        return try {
            val startTime = System.currentTimeMillis()
            httpUrlConn = URL(targetUrl)
                .openConnection() as HttpURLConnection
            httpUrlConn.requestMethod = "HEAD"
            val endTime = System.currentTimeMillis()
              connectionTime = endTime - startTime
              UrlDB(targetUrl,connectionTime,httpUrlConn.responseCode)
        } catch (e: Exception) {
            UrlDB(targetUrl,connectionTime,210)

        }

    }
}