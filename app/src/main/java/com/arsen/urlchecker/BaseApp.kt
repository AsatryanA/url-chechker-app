package com.arsen.urlchecker

import android.app.Application
import com.arsen.urlchecker.db.database.UrlDataBase

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        getDataBase()
    }

    fun getDataBase() = UrlDataBase.getInstance(this)
}