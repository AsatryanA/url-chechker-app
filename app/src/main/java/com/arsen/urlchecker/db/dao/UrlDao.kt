package com.arsen.urlchecker.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.arsen.urlchecker.db.UrlDB


@Dao
interface UrlDao {

    @Query("select  * From url ")
    fun getLiveUrl(): LiveData<List<UrlDB>>

    @Insert
    fun addUrl(url: UrlDB)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateUrl( url: UrlDB)

    @Query("UPDATE url SET availability = :url_availability ,connectionTime = :url_requestCode  WHERE link = :link")
    fun updateUrlField(link: String, url_requestCode : Long,url_availability: Int)

    @Delete
    fun deleteUrl(user: UrlDB)


}