package com.arsen.urlchecker.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.net.URL


@Entity(tableName = "url")
data class UrlDB(
    var link: String,
    var connectionTime : Long,
    var availability: Int = 0
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
