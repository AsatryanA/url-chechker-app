package com.arsen.urlchecker.db.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.arsen.urlchecker.db.UrlDB
import com.arsen.urlchecker.db.dao.UrlDao


@Database(entities = [UrlDB::class], version = 4)
abstract class UrlDataBase : RoomDatabase() {

    abstract fun urlDao(): UrlDao

    companion object {
        private var INSTANCE: UrlDataBase? = null

        fun getInstance(context: Context): UrlDataBase? {
            if (INSTANCE == null) {
                synchronized(UrlDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        UrlDataBase::class.java, "userDataBase"
                    ).fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

    }
}