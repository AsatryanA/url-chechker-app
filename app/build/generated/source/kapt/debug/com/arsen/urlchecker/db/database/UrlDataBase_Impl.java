package com.arsen.urlchecker.db.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import com.arsen.urlchecker.db.dao.UrlDao;
import com.arsen.urlchecker.db.dao.UrlDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class UrlDataBase_Impl extends UrlDataBase {
  private volatile UrlDao _urlDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(4) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `url` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `link` TEXT NOT NULL, `connectionTime` INTEGER NOT NULL, `availability` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"9f712dca373a4f36692eb58d56af7a9e\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `url`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsUrl = new HashMap<String, TableInfo.Column>(4);
        _columnsUrl.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsUrl.put("link", new TableInfo.Column("link", "TEXT", true, 0));
        _columnsUrl.put("connectionTime", new TableInfo.Column("connectionTime", "INTEGER", true, 0));
        _columnsUrl.put("availability", new TableInfo.Column("availability", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUrl = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUrl = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUrl = new TableInfo("url", _columnsUrl, _foreignKeysUrl, _indicesUrl);
        final TableInfo _existingUrl = TableInfo.read(_db, "url");
        if (! _infoUrl.equals(_existingUrl)) {
          throw new IllegalStateException("Migration didn't properly handle url(com.arsen.urlchecker.db.UrlDB).\n"
                  + " Expected:\n" + _infoUrl + "\n"
                  + " Found:\n" + _existingUrl);
        }
      }
    }, "9f712dca373a4f36692eb58d56af7a9e", "c0d9495d8de6953a6a48ae1d552ac6c6");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "url");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `url`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public UrlDao urlDao() {
    if (_urlDao != null) {
      return _urlDao;
    } else {
      synchronized(this) {
        if(_urlDao == null) {
          _urlDao = new UrlDao_Impl(this);
        }
        return _urlDao;
      }
    }
  }
}
