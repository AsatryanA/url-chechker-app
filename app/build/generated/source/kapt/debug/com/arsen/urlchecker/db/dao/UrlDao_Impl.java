package com.arsen.urlchecker.db.dao;

import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.InvalidationTracker.Observer;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import android.support.annotation.NonNull;
import com.arsen.urlchecker.db.UrlDB;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public class UrlDao_Impl implements UrlDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfUrlDB;

  private final EntityInsertionAdapter __insertionAdapterOfUrlDB_1;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfUrlDB;

  private final SharedSQLiteStatement __preparedStmtOfUpdateUrlField;

  public UrlDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUrlDB = new EntityInsertionAdapter<UrlDB>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `url`(`id`,`link`,`connectionTime`,`availability`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, UrlDB value) {
        stmt.bindLong(1, value.getId());
        if (value.getLink() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getLink());
        }
        stmt.bindLong(3, value.getConnectionTime());
        stmt.bindLong(4, value.getAvailability());
      }
    };
    this.__insertionAdapterOfUrlDB_1 = new EntityInsertionAdapter<UrlDB>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `url`(`id`,`link`,`connectionTime`,`availability`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, UrlDB value) {
        stmt.bindLong(1, value.getId());
        if (value.getLink() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getLink());
        }
        stmt.bindLong(3, value.getConnectionTime());
        stmt.bindLong(4, value.getAvailability());
      }
    };
    this.__deletionAdapterOfUrlDB = new EntityDeletionOrUpdateAdapter<UrlDB>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `url` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, UrlDB value) {
        stmt.bindLong(1, value.getId());
      }
    };
    this.__preparedStmtOfUpdateUrlField = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE url SET availability = ? ,connectionTime = ?  WHERE link = ?";
        return _query;
      }
    };
  }

  @Override
  public void addUrl(UrlDB url) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfUrlDB.insert(url);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateUrl(UrlDB url) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfUrlDB_1.insert(url);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteUrl(UrlDB user) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfUrlDB.handle(user);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateUrlField(String link, long url_requestCode, int url_availability) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateUrlField.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, url_availability);
      _argIndex = 2;
      _stmt.bindLong(_argIndex, url_requestCode);
      _argIndex = 3;
      if (link == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, link);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateUrlField.release(_stmt);
    }
  }

  @Override
  public LiveData<List<UrlDB>> getLiveUrl() {
    final String _sql = "select  * From url ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<UrlDB>>() {
      private Observer _observer;

      @Override
      protected List<UrlDB> compute() {
        if (_observer == null) {
          _observer = new Observer("url") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfLink = _cursor.getColumnIndexOrThrow("link");
          final int _cursorIndexOfConnectionTime = _cursor.getColumnIndexOrThrow("connectionTime");
          final int _cursorIndexOfAvailability = _cursor.getColumnIndexOrThrow("availability");
          final List<UrlDB> _result = new ArrayList<UrlDB>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final UrlDB _item;
            final String _tmpLink;
            _tmpLink = _cursor.getString(_cursorIndexOfLink);
            final long _tmpConnectionTime;
            _tmpConnectionTime = _cursor.getLong(_cursorIndexOfConnectionTime);
            final int _tmpAvailability;
            _tmpAvailability = _cursor.getInt(_cursorIndexOfAvailability);
            _item = new UrlDB(_tmpLink,_tmpConnectionTime,_tmpAvailability);
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            _item.setId(_tmpId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }
}
