package com.arsen.urlchecker.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/arsen/urlchecker/service/NetworkCall;", "", "()V", "checkIfURLExists", "Lcom/arsen/urlchecker/db/UrlDB;", "targetUrl", "", "app_debug"})
public final class NetworkCall {
    
    @org.jetbrains.annotations.Nullable()
    public final com.arsen.urlchecker.db.UrlDB checkIfURLExists(@org.jetbrains.annotations.NotNull()
    java.lang.String targetUrl) {
        return null;
    }
    
    public NetworkCall() {
        super();
    }
}