package com.arsen.urlchecker.db.dao;

import java.lang.System;

@android.arch.persistence.room.Dao()
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0005H\'J\u0014\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\n0\tH\'J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J \u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\'\u00a8\u0006\u0013"}, d2 = {"Lcom/arsen/urlchecker/db/dao/UrlDao;", "", "addUrl", "", "url", "Lcom/arsen/urlchecker/db/UrlDB;", "deleteUrl", "user", "getLiveUrl", "Landroid/arch/lifecycle/LiveData;", "", "updateUrl", "updateUrlField", "link", "", "url_requestCode", "", "url_availability", "", "app_debug"})
public abstract interface UrlDao {
    
    @org.jetbrains.annotations.NotNull()
    @android.arch.persistence.room.Query(value = "select  * From url ")
    public abstract android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> getLiveUrl();
    
    @android.arch.persistence.room.Insert()
    public abstract void addUrl(@org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB url);
    
    @android.arch.persistence.room.Insert(onConflict = android.arch.persistence.room.OnConflictStrategy.REPLACE)
    public abstract void updateUrl(@org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB url);
    
    @android.arch.persistence.room.Query(value = "UPDATE url SET availability = :url_availability ,connectionTime = :url_requestCode  WHERE link = :link")
    public abstract void updateUrlField(@org.jetbrains.annotations.NotNull()
    java.lang.String link, long url_requestCode, int url_availability);
    
    @android.arch.persistence.room.Delete()
    public abstract void deleteUrl(@org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB user);
}