package com.arsen.urlchecker;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0016\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\nJ\u001e\u0010\u0015\u001a\u00020\u00102\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0017j\b\u0012\u0004\u0012\u00020\n`\u0018J\u000e\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\nJ.\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t2\u0016\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0017j\b\u0012\u0004\u0012\u00020\n`\u00182\u0006\u0010\u001b\u001a\u00020\u001cR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0007\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u001d"}, d2 = {"Lcom/arsen/urlchecker/MainViewModel;", "Landroid/arch/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "repository", "Lcom/arsen/urlchecker/MainRepository;", "urlsLiveData", "Landroid/arch/lifecycle/LiveData;", "", "Lcom/arsen/urlchecker/db/UrlDB;", "getUrlsLiveData", "()Landroid/arch/lifecycle/LiveData;", "setUrlsLiveData", "(Landroid/arch/lifecycle/LiveData;)V", "openDialog", "", "context", "Landroid/content/Context;", "openRemoveDialog", "url", "refreshAllList", "urlList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "removeUrlItem", "sortListByChoose", "sortType", "", "app_debug"})
public final class MainViewModel extends android.arch.lifecycle.AndroidViewModel {
    private final com.arsen.urlchecker.MainRepository repository = null;
    @org.jetbrains.annotations.Nullable()
    private android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> urlsLiveData;
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> getUrlsLiveData() {
        return null;
    }
    
    public final void setUrlsLiveData(@org.jetbrains.annotations.Nullable()
    android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> p0) {
    }
    
    public final void removeUrlItem(@org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB url) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.arsen.urlchecker.db.UrlDB> sortListByChoose(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.arsen.urlchecker.db.UrlDB> urlList, int sortType) {
        return null;
    }
    
    public final void refreshAllList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.arsen.urlchecker.db.UrlDB> urlList) {
    }
    
    public final void openDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void openRemoveDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB url) {
    }
    
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}