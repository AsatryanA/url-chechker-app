package com.arsen.urlchecker;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\nH\u0002J\u000e\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u00162\u0006\u0010\u0011\u001a\u00020\u0014H\u0002J\u001e\u0010\u0017\u001a\u00020\u00102\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0019j\b\u0012\u0004\u0012\u00020\n`\u001aJ\u000e\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\nJ\u0010\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\nH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R(\u0010\u0007\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u001e"}, d2 = {"Lcom/arsen/urlchecker/MainRepository;", "", "()V", "networkCall", "Lcom/arsen/urlchecker/service/NetworkCall;", "urlDao", "Lcom/arsen/urlchecker/db/dao/UrlDao;", "urlList", "Landroid/arch/lifecycle/LiveData;", "", "Lcom/arsen/urlchecker/db/UrlDB;", "getUrlList", "()Landroid/arch/lifecycle/LiveData;", "setUrlList", "(Landroid/arch/lifecycle/LiveData;)V", "addUrl", "", "url", "checkURL", "address", "", "getResultOfUrl", "Lio/reactivex/Observable;", "refreshList", "list", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "removeUrl", "updateUrl", "urlObject", "app_debug"})
public final class MainRepository {
    private static com.arsen.urlchecker.service.NetworkCall networkCall;
    private static com.arsen.urlchecker.db.dao.UrlDao urlDao;
    @org.jetbrains.annotations.Nullable()
    private static android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> urlList;
    public static final com.arsen.urlchecker.MainRepository INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> getUrlList() {
        return null;
    }
    
    public final void setUrlList(@org.jetbrains.annotations.Nullable()
    android.arch.lifecycle.LiveData<java.util.List<com.arsen.urlchecker.db.UrlDB>> p0) {
    }
    
    public final void removeUrl(@org.jetbrains.annotations.NotNull()
    com.arsen.urlchecker.db.UrlDB url) {
    }
    
    public final void checkURL(@org.jetbrains.annotations.NotNull()
    java.lang.String address) {
    }
    
    public final void refreshList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.arsen.urlchecker.db.UrlDB> list) {
    }
    
    private final io.reactivex.Observable<com.arsen.urlchecker.db.UrlDB> getResultOfUrl(java.lang.String url) {
        return null;
    }
    
    private final void addUrl(com.arsen.urlchecker.db.UrlDB url) {
    }
    
    private final void updateUrl(com.arsen.urlchecker.db.UrlDB urlObject) {
    }
    
    private MainRepository() {
        super();
    }
}